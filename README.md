# The Flying Knight

An honorary tavern made after the death of a well-loved member of our
community, Knight Flier.

## Description

On the 24th of March 2019, a beloved member of Lexy's community passed away due
to bone cancer complications. The Flying Knight tavern was built in his memory,
and we hope you enjoy our labor of love!

Version 2.0.0 introduces a completely redone and re-imagined tavern made by
Evertiro. A full list of changes can be found in the changelog but I recommend
you take a look for yourself. This won't be the last update to this mod and it
might not even be the biggest as we have some pretty crazy plans for some stuff.
No spoilers just yet though ; )

## Contributing

The Flying Knight is a community-driven mod. If you are interested in helping
improve the tavern, or have a suggestion for something we can add, send a PM
to Lexy, Nathan or Evertiro on Lexy's Discord server.

We also want to see your visits to the tavern! Post your favorite pictures of
you and your followers enjoying a nice meal around the feast table (or a nice
bottle of mead)!

## Compatibility

The Flying Knight should be compatible with anything that doesn't modify the
same part of the Rift. To date, the only known mod that directly conflicts
with TFK is [Inns and Taverns SE], although we provide an optional
compatibility patch.

## Thanks

We would like to extend our sincere thanks to the following awesome people.
Without you, TFK wouldn't exist!

* [Diana TES GotH] and her son [Sean] for making the amazing custom tavern sign.
* [Billyro] for making the model for Ser Knight's sword.
* [Sthaagg] for his assistance with integrating Keep It Clean.
* Aardvark Chips for the model for the oil painting hanging in the banquet hall.
* [Ic0nic0de] for the patch to add Dragonborn Museum shipping containers outside
of The Flying Knight.
* [Tortle2Tortle2Turtle] for creating the Alternate Perspective patch.

### Additional Thanks

While The Flying Knight is no longer based on Inns and Taverns, the following
people graciously gave permission for it to be the original base of our mod.
Without their work, Nathan couldn't have made the original version of
The Flying Knight, and we wouldn't be where we are today.

* [phoenixfabricio] for the [Inns and Taverns SE] port.
* [menathradiel] for the original [Inns and Taverns] mod for LE.

## Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/skyrim/the-flying-knight)!

[Diana TES GotH]: https://www.nexusmods.com/skyrim/users/19386019
[Sean]: https://shumburg.com/
[Billyro]: https://www.nexusmods.com/skyrimspecialedition/users/1734939
[Sthaagg]: https://www.nexusmods.com/skyrimspecialedition/users/4064736
[Ic0nic0de]: https://www.nexusmods.com/skyrimspecialedition/users/45243387
[Tortle2Tortle2Turtle]: https://www.nexusmods.com/skyrimspecialedition/users/42492840
[phoenixfabricio]: https://www.nexusmods.com/skyrimspecialedition/users/19825204
[Inns and Taverns SE]: https://www.nexusmods.com/skyrimspecialedition/mods/12223
[menathradiel]: https://www.nexusmods.com/skyrim/users/3721994
[Inns and Taverns]: https://www.nexusmods.com/skyrim/mods/28283
