# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.10] - 2023-08-17

### Added

- Option to install without an alternate start mod.

### Changed

- FOMOD cleanup.

## [2.0.9] - 2021-12-15

### Fixed

- FOMOD support for Vortex.

## [2.0.8] - 2021-12-15

### Fixed

- Modular installer support on Vortex.

## [2.0.7] - 2021-12-14

### Added

- Alternate start mod choice for guide users.

## [2.0.6] - 2021-12-14

### Fixed

- Merged build for guide users.

## [2.0.5] - 2021-12-14

### Added

- Patch for Alternate Perspective,

### Fixed

- Quest provider for Honouring the Fallen.

## [2.0.4b] - 2020-12-01

### Fixed

- Typo in FOMOD modular option.

## [2.0.4] - 2020-04-15

### Fixed

- Blackface bug with guide install option.

## [2.0.3] - 2020-04-03

### Added

- Patch to add Dragonborn Museum shipping crates outside the tavern (props: Ic0nic0de).

## [2.0.2] - 2019-12-04

### Fixed

- Naming issue with mod EDIDs (#7).

## [2.0.1] - 2019-12-03

### Fixed

- Landscaping issue created when the road was added.

## [2.0.0] - 2019-12-03

### Added

- Pre-made merged copy for use with Lexy's LOTD and LOTD Plus guides.
- Completely new tavern (#2).
- Bumbles, the friendly frost troll (#1).
- In-game Ledger telling the tavern history and credits.
- Proper support for Keep It Clean.
- Patch for Alternate Start - Live Another Life to allow starting at TFK.

### Changed

- Cleaned up and reworked exterior landscaping.

### Removed

- Hostile spawns in proximity to TFK.

## [1.03.0a] - 2019-09-29

### Fixed

- Missing signs textures.

## [1.02.0] - 2019-09-26

### Added

- Custom AI package for Ser Knight.
- Merged in Teabag's Hero patch.
- Merged in Teabag's animal patch.
- Bash Tags.

### Changed

- Changed Ser Knight to Vendor Pawnbroker class.
- Packed assets into a BSA.

### Fixed

- Ownership errors
- Allow Ser Knight to function as a proper inn keeper.
- Outdated references to HeartwoodInn and InnsAndTaverns.
- Removed a few wild edits.

## [1.01.0] - 2019-07-10

### Fixed

- Removed some old, unused data.

## [1.0.0] - 2019-07-09

- Initial release
